const axios = require('axios');
const apiHost = process.env.HOST || 'http://localhost:5000';

const CREATION_ENDPOINT = `${apiHost}/rest/save`;
const READ_ENDPOINT = `${apiHost}/rest/load`;

const postCreate = payload => axios.post(CREATION_ENDPOINT, payload);
const read = id => axios.get(`${READ_ENDPOINT}/${id}`);

module.exports = {
  postCreate, 
  read
}