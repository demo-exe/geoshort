import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import styled from 'styled-components';
import './App.css';
import MapScreen from './components/MapScreen';
import ViewScreen from './components/ViewScreen';

const AppContainer = styled.div`
  width: 100%;
  text-align: center;
`;

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <AppContainer>
            <Route path="/v/:id" component={ViewScreen} />
            <Route exact path="/" component={MapScreen} />
          </AppContainer>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
