#!/bin/sh

echo "Setting up mongodb database"
docker volume create mongodbdata
docker run -d -p 27017:27017 -v mongodbdata:/data/db mongo

echo "Installing backend dependencies"
cd backend && pip install -U pymongo flask-cors && cd ..

echo "Setting up frontend client"
cd frontend && npm install

echo "Starting environment (frontend client + service)"
npm run startDev


